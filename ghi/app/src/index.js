import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


async function loadWardrobe() {
  const responseHats = await fetch('http://localhost:8090/api/hats/');
  const responseShoes = await fetch("http://localhost:8080/api/shoes");
  if (responseHats.ok && responseShoes.ok) {
    const hatData = await responseHats.json();
    const shoeData = await responseShoes.json();
    root.render(
      <React.StrictMode>
        <App hats={hatData.hats} shoes={shoeData.shoes} />
      </React.StrictMode>
    );
  } else {
    console.error(responseHats, responseShoes);
  }
}
loadWardrobe();
