import React from "react";


function HatsList(props) {

    async function deleteHat(hat) {
        const hatID = hat.id;
        const response = await fetch(`http://localhost:8090/api/hats/${hatID}/`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        });
        if (response.ok) {
            window.location.reload();
        };
    }
    return (
        <table className="table table-striped align-middle mt-5">
            <thead>
                <tr>
                    <th>Photo</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Fabric</th>
                    <th>Closet</th>
                    <th>Section</th>
                    <th>Shelf</th>
                    <th>Delete?</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map((hat) => {
                    return (
                        <tr key={hat.href}>
                            <td><img src={hat.photo_url} className="img-thumbnail hats" alt={hat.style}></img></td>
                            <td>{hat.style}</td>
                            <td>{hat.color}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.closet}</td>
                            <td>{hat.section}</td>
                            <td>{hat.shelf}</td>
                            <td><button type="button" className="btn btn-danger" onClick={() => deleteHat(hat)} value={hat.id}>Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>

    );
}
export default HatsList;
