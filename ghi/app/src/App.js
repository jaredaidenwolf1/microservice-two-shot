import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import MainPage from './MainPage';
import HatsList from './HatsList';
import AddHatForm from './AddHatForm';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';

function App(props) {
  if (props.hats === undefined && props.shoes === undefined) {
    return null;
  }
 
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
            <Route path="hats">
              <Route index element={<HatsList hats={props.hats} />} />
              <Route path="new" element={<AddHatForm />}></Route>
            </Route>
            <Route path="shoes">
              <Route index element={<ShoesList shoes={props.shoes}/>} />
              <Route path="new" element={<ShoeForm />}></Route>
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
