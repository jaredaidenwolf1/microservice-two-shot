from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.closet_name


class Hat(models.Model):
    color = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200)
    style = models.CharField(max_length=200)
    photo_url = models.URLField()
    created = models.DateTimeField(auto_now_add=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hat",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.style

    def get_api_url(self):
        return reverse(
            "api_show_hat",
            kwargs={
                "pk": self.pk,
            },
        )

    class Meta:
        ordering = (
            "color",
            "fabric",
            "style",
        )
